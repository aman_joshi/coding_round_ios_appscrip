//
//  FoodItem.swift
//  CodingRound
//
//  Created by Aman Joshi on 06/02/21.
//  Copyright © 2021 Aman Joshi. All rights reserved.
//

import SwiftUI

enum FoodItemCategory:String,CaseIterable {
    case banner
    case pizza = "Pizza's"
    case burger = "Burgers"
}

struct FoodItem {
    var id:UUID
    var image:UIImage
    var name:String
    var category:FoodItemCategory
    var description:String
    var price:Int
}


let foodItemsList = [

    FoodItem(id: UUID(), image: UIImage(named: "farmhouse")!, name: "A Medium Farmhouse Pizza with Cheese Burst", category: .pizza, description: "A pizza that goes ballistic on veggies!", price: 599),
    
    FoodItem(id: UUID(), image: UIImage(named: "margherita")!, name: "Margherita", category: .pizza, description: "A hugely popular margherita, with a deliciously tangy single cheese topping.", price: 499),
    
    FoodItem(id: UUID(), image: UIImage(named: "doubleCheeseMargherita")!, name: "Double Cheese Margherita", category: .pizza, description: "The ever-popular Margherita - loaded with extra cheese... ", price: 699),
    
    FoodItem(id: UUID(), image: UIImage(named: "MexicanGreenWave")!, name: "Mexican Green Wave", category: .pizza, description: "Chunky paneer with crisp capsicum and spicy red pepper - quite a mouthful!", price: 399),
    
    
    FoodItem(id: UUID(), image: UIImage(named: "LugerBurger")!, name: "Luger Burger", category: .burger, description: "It takes a mighty burger for us not to order a steak at Peter Luger Steakhouse, and this is that burger.", price: 199),
    
    FoodItem(id: UUID(), image: UIImage(named: "LePigeonBurger")!, name: "Le Pigeon Burger", category: .burger, description: "The grilled Le Pigeon Burger has a nice, smoky flavor that is enhanced with each topping. Gabriel Rucker layers it with Tillamook white cheddar, lettuce slaw, and pickled onions along with some ketchup, mayo, and mustard.", price: 149),
    
    FoodItem(id: UUID(), image: UIImage(named: "DoubleAnimalStyle")!, name: "Double Animal Style", category: .burger, description: "Order this one off the secret menu that’s not-so-secret at all. Leveling up to “Animal Style” will give you added pickles, extra spread with grilled onions, and they’ll fry some mustard onto your patties.", price: 99),
    
    FoodItem(id: UUID(), image: UIImage(named: "WhiskeyKingBurger")!, name: "Whiskey King Burger", category: .burger, description: "Jose Garces serves up this $26 plate of insanity over at Village Whiskey in Philly. It’s topped with maple bourbon glazed cipollini, bleu cheese, bacon, and a bit of foie gras. Sweet, fatty, and decadent.", price: 249),
    
    
    FoodItem(id: UUID(), image: UIImage(named: "Dominos_logo")!, name: "", category: .banner, description: "", price: 0),
    
    FoodItem(id: UUID(), image: UIImage(named: "mcdonalds")!, name: "", category: .banner, description: "", price: 0),
    
    FoodItem(id: UUID(), image: UIImage(named: "kfc")!, name: "", category: .banner, description: "", price: 0),
    

]


