//
//  ContentView.swift
//  CodingRound
//
//  Created by Aman Joshi on 06/02/21.
//  Copyright © 2021 Aman Joshi. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    private var viewModel = FoodListItemViewModel()
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                    FoodItemRow(categoryName: nil, foodItems: viewModel.foodItems(filterby: .banner))
                    FoodItemRow(categoryName: FoodItemCategory.pizza.rawValue, foodItems: viewModel.foodItems(filterby: .pizza)).padding(.vertical)
                    FoodItemRow(categoryName: FoodItemCategory.burger.rawValue, foodItems: viewModel.foodItems(filterby: .burger)).padding(.vertical)
                    Spacer()
                }
            }
            .navigationBarTitle("Food Items")
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
