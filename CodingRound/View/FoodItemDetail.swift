//
//  FoodItemDetail.swift
//  CodingRound
//
//  Created by Aman Joshi on 07/02/21.
//  Copyright © 2021 Aman Joshi. All rights reserved.
//

import SwiftUI

struct FoodItemDetail: View {
    
    var foodItem:FoodItem
    @State var isImageBlur = false
    
    var body: some View {
        VStack {
            FoodItemView(foodItem: foodItem,imageWidth: UIScreen.main.bounds.width * 0.9, imageHeight: UIScreen.main.bounds.height * 0.5, isBlur: $isImageBlur)
            
            Text("Price - \(foodItem.price)INR").font(.subheadline).padding(.top,8)
            Text(foodItem.description).font(.subheadline).lineLimit(nil).padding(.vertical,8)
            
            Button(action: {
                self.isImageBlur.toggle()
            }) {
                Text("Blur Image").font(.title).fontWeight(.bold)
            }
            .padding()
            .background(Color.gray.opacity(0.3))
            .cornerRadius(10.0)
            
            Spacer()
        }
    }
}

