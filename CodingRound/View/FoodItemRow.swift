//
//  FoodItemRow.swift
//  CodingRound
//
//  Created by Aman Joshi on 07/02/21.
//  Copyright © 2021 Aman Joshi. All rights reserved.
//

import SwiftUI

struct FoodItemRow: View {
    
    var categoryName:String?
    var foodItems:[FoodItem]
    @State var isImageBlur:Bool = false
    
    var body: some View {
        VStack(alignment: .leading) {
            if categoryName != nil {
                Text(categoryName!)
                .font(.headline).fontWeight(.bold).padding(.horizontal)
            }
            
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(alignment: .top, spacing: 0) {
                    ForEach(foodItems, id: \.id) { item in
                        NavigationLink(destination: FoodItemDetail(foodItem: item)) {
                            FoodItemView(foodItem: item,imageWidth: item.name == "" ? UIScreen.main.bounds.width * 0.9 : UIScreen.main.bounds.width * 0.7, imageHeight: 180, isBlur: self.$isImageBlur)
                        }
                    .buttonStyle(PlainButtonStyle())
                    }
                }
            }
        }
        
    }
}

