//
//  FoodItemView.swift
//  CodingRound
//
//  Created by Aman Joshi on 07/02/21.
//  Copyright © 2021 Aman Joshi. All rights reserved.
//

import SwiftUI

struct FoodItemView: View {
    
    var foodItem:FoodItem
    var imageWidth:CGFloat
    var imageHeight:CGFloat
    @Binding var isBlur: Bool
    
    var body: some View {
        VStack(alignment: .center) {
            Image(uiImage: foodItem.image)
                .resizable()
                .frame(width: imageWidth, height: imageHeight)
                .cornerRadius(5)
                .blur(radius: isBlur ? 10.0 : 0)
            if foodItem.name != "" {
                Text(foodItem.name)
                    .font(.caption).lineLimit(nil)
            }
        }
        .padding(.leading, 15)
    }
}

