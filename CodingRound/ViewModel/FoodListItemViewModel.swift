//
//  FoodListItemViewModel.swift
//  CodingRound
//
//  Created by Aman Joshi on 07/02/21.
//  Copyright © 2021 Aman Joshi. All rights reserved.
//

import Foundation
import SwiftUI

class FoodListItemViewModel {
        
    func foodItems(filterby catergory:FoodItemCategory) -> [FoodItem] {
        return foodItemsList.filter({$0.category == catergory})
    }
}
